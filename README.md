# it_regional_innovation

## Description
Innovation depends on several factors: research insitutions, acedemia, companies and "human resources", which includes talented researchers, entrepreneurs, students...
This datasets is a collection open data from a variety of sources, filteret to Italy and georeferenced to NUTS-2 level. Whenever possible also NUTS-3 level.

## Project status
Ongoing development. 
Currently available:
* NUTS2 and NUTS2 maps in .csv format
* NUTS-2 and NUTS-3 names 
* Universities, phd, masters

Under development:
* Universities: students, medical schools
* spin-offs
* startups
* Horizon2020 grants SME-instrument, Marie Curie and others.  

## Authors and acknowledgment
Author: Fabio Morea, Area Science Park, Italy
contact: fabio.morea@areasciencepark.it

## License
Creative Commons


